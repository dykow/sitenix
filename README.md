# Sitenix

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Unix-like OS
* [Docker](https://docs.docker.com/install/)
* [Docker-compose](https://docs.docker.com/compose/install/)

### Installing

A step by step series of examples that tell you how to get a development env running

Clone repo:
```
git clone git@gitlab.com:dykow/sitenix.git
```
Move into *sitenix* directory:
```
cd sitenix
```
Set *your_mysql_root_password* for your database **root** and database's *name* (*laravel* by default) in **docker-compose.yml**:
```
nano docker-compose.yml
```
Set permissions on the project directory so that it is owned by your **non-root** user:
```
sudo chown -R your_system_username:your_system_username $(pwd)
```
Make *build.sh* executable:
```
chmod +x build.sh
```
Run *build.sh*:
```
bash build.sh
```
Edit **DB_HOST** (set to *db*), **DB_DATABASE** (set *laravel* database's name), **DB_USERNAME** (set *laraveluser* name) and **DB_PASSWORD** (*your_laravel_db_password*):
```
docker-compose exec app nano .env
```
Next, set the application key for the Laravel application with the php ```artisan key:generate``` command:
```
docker-compose exec app php artisan key:generate
```
You now have the environment settings required to run your application. To cache these settings into a file, which will boost your application's load speed, run: 
```
docker-compose exec app php artisan config:cache
```

**Creating a User for MySQL**

To create a new user, execute an interactive bash shell:
```
docker-compose exec db bash
```
Inside the container, log into the MySQL **root** administrative account:
```
mysql -u root -p
```
You will be prompted for the *your_mysql_root_password* you set for the MySQL root account during installation in your docker-compose file.

Create the user account that will be allowed to access this database. Our username will be *laraveluser*, though you should replace it with name specified in *.env*.
In MySQL prompt:
```
GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'your_laravel_db_password';
```
Flush the privileges to notify the MySQL server of the changes:
```
FLUSH PRIVILEGES;
```
Exit MySQL:
```
EXIT;
```
Finally, exit the container:
```
exit
```

**Migrating Data and Working with the Tinker Console**
Test the connection to MySQL by running the Laravel artisan migrate command, which creates a migrations table in the database from inside the container:
```
docker-compose exec app php artisan migrate
```
If you get an error check if your db credentials are correct.

As a final step, visit http://your_server_ip in the browser.
Check your server's ip:
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' webserver
```

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [Bulma](https://bulma.io/) - CSS framework
* MySQL - database

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

